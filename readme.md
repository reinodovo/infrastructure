# Infrastructure

## Loading dotenv files

During this process, you will need to source multiple .env files that may have special characters, to do this, run `source load-env.sh` and use the `load-env` command instead.

## Docker

Installing docker through `snap` might generate some problems, so install it from docker repository following [this guide](https://docs.docker.com/engine/install/ubuntu/).

## Installing k3s

`curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644 --kube-apiserver-arg 'service-node-port-range=1-65535' --docker`

You will need to run `export KUBECONFIG=/etc/rancher/k3s/k3s.yaml` and add it to your `.bashrc`

Run `kubectl get all --all-namespaces` to make sure that everything is ok.

## Helm

You will need `helm` to setup a few services, you can install it with `snap install helm --classic`

### RabbitMQ

You will need to create a `.rabbitmq.env` file with the following environment variables

| Variable                       | Explanation                                  |
|--------------------------------|----------------------------------------------|
| `RABBITMQ_USERNAME`            | Username to connect to RabbitMQ              |
| `RABBITMQ_PASSWORD`            | Password to connect to RabbitMQ              |
| `RABBITMQ_MANAGEMENT_HOSTNAME` | Hostname to access the management console UI |

```
load-env .rabbitmq.env
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install rabbit bitnami/rabbitmq -f rabbitmq.yml \
    --set auth.username=$RABBITMQ_USERNAME \
    --set auth.password=$RABBITMQ_PASSWORD \
    --set ingress.hostname=$RABBITMQ_MANAGEMENT_HOSTNAME
```

Now you can access the management UI using the provided hostname, and pods inside your k3s cluster can talk to RabbitMQ at `rabbit-rabbitmq.default.svc.cluster.local`. Check the output of the RabbitMQ installation to make sure that's the right host.

#### Bot stream

The bots need a specific stream to be able to talk to each other. Enter the queue by going to `rabbitmq.{yourdomain}` and create a new stream with `max-age` set.

### MongoDB

You will need to create a `.mongodb.env` file with the following environment variables

| Variable           | Explanation                    |
|--------------------|--------------------------------|
| `MONGODB_USERNAME` | Username to connect to MongoDB |
| `MONGODB_PASSWORD` | Password to connect to MongoDB |

```
load-env .mongodb.env
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install mongo bitnami/mongodb -f mongodb.yml \
    --set auth.rootUser=$MONGODB_USERNAME \
    --set auth.rootPassword=$MONGODB_PASSWORD
```

Just like RabbitMQ, the database is available at `mongo-mongodb.default.svc.cluster.local`, but check the command output to make sure.

#### External Access

You will need to configure a `IngressRouteTCP` configuration, but first, traefik needs to be able to serve TCP connections on port 27017. To do this, copy the contents of `traefik-config.yaml` to `/var/lib/rancher/k3s/server/manifests/traefik-config.yaml`.

You can check that the new configuration as applied successfully by running `kubectl get all --all-namespaces` and making sure that the traefik service now exposes port `27017`.

Now you can run `kubectl apply -f mongo-ingress.yml` and then use a MongoDB client like `mongodb-compass` or `MongoLime` for the iOS.

#### Migrate from an existing server

Use `mongodump` and `mongorestore` from `mongodb-utils` package to migrate files from a server to another.

```
mongodump    --uri="old-server-uri" --gzip --archive="path-to-archive-file"
mongorestore --uri="new-server-uri" --gzip --archive="path-to-archive-file"
```
